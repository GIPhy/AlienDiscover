[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/)
[![JAVA](https://img.shields.io/badge/Java-13-be0032?logo=java)](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

# AlienDiscover


_AlienDiscover_ is a command line program written in [Java](https://docs.oracle.com/en/java/) that quickly infer 3' alien oligonucleotide sequence(s) (e.g. adapters, primers, ...) from ([Illumina](https://www.illumina.com/)) short high-thoughput sequencing (HTS) reads, without any prior knowledge. _AlienDiscover_ is an alternative to the tool _Minion_ from the _Kraken_ suite ([Davis et al. 2013](https://doi.org/10.1016/j.ymeth.2013.06.027)). Inferred alien oligonucleotide sequence(s) can next be clipped using e.g. [AlienTrimmer](https://gitlab.pasteur.fr/GIPhy/AlienTrimmer).


## Installation

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/AlienDiscover.git
```


## Compilation and execution

The source code of _AlienDiscover_ is inside the _src_ directory. It requires **Java 13** (or higher) to be compiled. 

#### Building an executable jar file

On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (**13** or higher) installed, a Java executable jar file can be created by typing the following command lines inside the _src_ directory:

```bash
javac AlienDiscover.java
echo Main-Class: AlienDiscover > MANIFEST.MF 
jar -cmvf MANIFEST.MF AlienDiscover.jar AlienDiscover.class 
rm MANIFEST.MF AlienDiscover.class 
```

This will create the executable jar file `AlienDiscover.jar` that can be run with the following command line model:

```bash
java -jar AlienDiscover.jar [options]
```

#### Building a native code binary

On computers with [GraalVM](https://www.graalvm.org/downloads/) installed, a native executable can be built. In a command-line window, go to the _src_ directory, and type:

```bash
javac AlienDiscover.java 
native-image AlienDiscover AlienDiscover
rm AlienDiscover.class
```
This will create the native executable `AlienDiscover` that can be run with the following command line model:
```bash
./AlienDiscover [options]
```


## Usage

Run _AlienDiscover_ without option to read the following documentation:

```
AlienDiscover 

 Fast inference of alien oligonucleotide sequence(s) (adapters, primers, ...) from FASTQ file

 USAGE:
   AlienRemover -i <FASTQ>  [-m <int>]  [-s <float>]  [-n <int>]  [-l <int>]  [-v]  [-h]

 OPTIONS:
    -i <infile>   FASTQ-formatted  input file  (mandatory);  filename should end
                  with .gz when gzipped (mandatory)
    -m <integer>  number of FASTQ blocks  to read to determine  the maximum read
                  length (default: 1000)
    -s <float>    position quantile to start enumerating k-mers (default: 0.75)
    -n <integer>  number of FASTQ blocks  to read to look for the alien sequence
                  (default: 100000)
    -l <integer>  minimum length of the inferred alien sequence(s) (default: 40)
    -v            verbose mode (default: not set)
    -h            prints this help and exit
```


## Notes

* The aim of _AlienDiscover_ is to find the longest substrings that occur "too" many times in the 3' regions of the input HTS reads. First, _AlienDiscover_ parses the _m_ (=&nbsp;1,000 by default; see option `-m`) first HTS reads to determine their (original) length _L_. Next, _AlienDiscover_ enumerates the number of occurrences of every _k_-mer (_k_ = 15) in the (_s_&nbsp;&times;&nbsp;_L_)<sup>th</sup> last positions of each HTS read (by default, _s_&nbsp;=&nbsp;0.75; see option `-s`). As the length of the (unknown) alien oligonucleotide sequence is expected to be at most _L_&nbsp;bps, _AlienDiscover_ then considers the _L_&nbsp;&minus;&nbsp;_k_ most frequent _k_-mers as alien ones, and finally looks for the longest subsequences that contain only alien _k_-mers within the _n_ (=&nbsp;100,000 by default; see option `-n`) first HTS reads. _AlienDiscover_ returns these alien subsequences when their length is higher than a fixed cutoff (i.e. 40 bps; see option `-l`).

* Input HTS reads should be in FASTQ format (gzipped files are accepted). Every inferred alien oligonucleotide sequence is written in `stdout` in FASTA format. When setting option `-v`, progress details are written in `stderr`.

* Of important note, when the input HTS reads were previously clipped (i.e. they do not share any alien oligonucleotide sequence), _AlienDiscover_ can infer a non-exogenous oligonucleotide sequence (often derived from a repeated region). In general, accurate results are observed when input (non-clipped) HTS reads are of length at least 100 bps.



## Examples

##### Example 1 

The following [Bash](https://www.gnu.org/software/bash/) command lines enable to download and uncompress the two compressed FASTQ files associated to the run accession [SRR10010605](https://www.ncbi.nlm.nih.gov/sra/SRR10010605):

```bash
EBIURL="https://ftp.sra.ebi.ac.uk/vol1/fastq";
wget $EBIURL/SRR100/005/SRR10010605/SRR10010605_1.fastq.gz ;
wget $EBIURL/SRR100/005/SRR10010605/SRR10010605_2.fastq.gz ;
```

The downloaded FASTQ files _SRR10010605_1.fastq.gz_ (561 Mb) and _SRR10010605_2.fastq.gz_ (683 Mb) correspond to the 2&times;250 bps whole genome sequencing of a _Bordetella pertusis_ strain.

_AlienDiscover_ can be used to infer the putative alien oligonucleotide sequences occurring within these (non-clipped) FASTQ files using the following command lines: 

```bash
AlienDiscover  -i SRR10010605_1.fastq.gz 
AlienDiscover  -i SRR10010605_2.fastq.gz
```

After few seconds, these two command lines return the following alien oligonucleotide sequences: 

```
>alien SRR10010605_1.fastq.gz
AGATCGGAAGAGCACACGTCTGAACTCCAGTCACATTCAGAAATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAAAAAA

>alien SRR10010605_2.fastq.gz
GAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTGCCTCTATGTGTAGATCTCGG
```

Interestingly, these two oligonucleotide sequences are quite identical to some [Illumina](https://www.illumina.com/) [_TruSeq CD index_](https://support-docs.illumina.com/SHARE/AdapterSeq/Content/SHARE/AdapterSeq/TruSeq/CDIndexes.htm) technical sequences (rc = reverse complement):

```
>alien SRR10010605_1.fastq.gz
AGATCGGAAGAGCACACGTCTGAACTCCAGTCACATTCAGAAATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAAAAAA
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
AGATCGGAAGAGCACACGTCTGAACTCCAGTCACattcagaaATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAAAAAA
<-------------------------------><-------------------------------]<------------>
        Truseq Adapter 1           3' Index 1 (i7) Adapter D705       poly-A      

>SRR10010605_2.fastq.gz
GAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTGCCTCTATGTGTAGATCTCGG
 ||||||||||||||||||||||||||||||||||||||||||||||||||||||
 AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTgcctctatGTGTAGATCTCGGTGGTCGCCGTATCATT
 <------------------------------->[----------------------------------->
         Truseq Adapter 2            5' rc Index 2 (i5) Adapter D502         
```

##### Example 2 

The following [Bash](https://www.gnu.org/software/bash/) command lines enable to download and uncompress the two compressed FASTQ files associated to the run accession [SRR6027828](https://www.ncbi.nlm.nih.gov/sra/SRR6027828):

```bash
EBIURL="https://ftp.sra.ebi.ac.uk/vol1/fastq";
wget $EBIURL/SRR602/008/SRR6027828/SRR6027828_1.fastq.gz ;
wget $EBIURL/SRR602/008/SRR6027828/SRR6027828_2.fastq.gz ;
```

The downloaded FASTQ files _SRR6027828_1.fastq.gz_ (355 Mb) and _SRR6027828_2.fastq.gz_ (418 Mb) correspond to the 2&times;300 bps whole genome sequencing of a _Vibrio cholerae_ strain.

As previously, _AlienDiscover_ can be used to infer the putative alien oligonucleotide sequences occurring within these (clipped/trimmed) FASTQ files using the following command lines: 

```bash
AlienDiscover  -i SRR6027828_1.fastq.gz 
AlienDiscover  -i SRR6027828_2.fastq.gz
```

leading to the following outputs: 

```
>alien SRR6027828_1.fastq.gz
TGTCCCTTATACACATCCCCGAGCCCACGAGACATCTCAGGATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAA

>alien SRR6027828_2.fastq.gz
CTGTCCCTTATACACCTCTGACGCTGCCGACGATACCCCTTGT
```

As shown below, these two oligonucleotide sequences are quite similar to some [Illumina](https://www.illumina.com/) [_Nextera DNA index_](https://support-docs.illumina.com/SHARE/AdapterSeq/Content/SHARE/AdapterSeq/Nextera/DNAIndexesNXT.htm) technical sequences (rc = reverse complement):

```
>SRR6027828_1.fastq.gz
 TGTCCCTTATACACATCCCCGAGCCCACGAGACATCTCAGGATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAA
 |||| |||||||||||| |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
CTGTCTCTTATACACATCTCCGAGCCCACGAGACatctcaggATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAA
<-----------------><---------------------------------------------><-------->
  Nextera Adapter           Index 1 (i7) Adapter [H/N]715           poly-A 
             
SRR6027828_2.fastq.gz
CTGTCCCTTATACACCTCTGACGCTGCCGACGATACCCCTTGT
||||| ||||||||| |||||||||||||||||||| ||||||
CTGTCTCTTATACACATCTGACGCTGCCGACGAtactccttGTGTAGATCTCGGTGGTCGCCGTATCATT
<-----------------><------------------------------------------------->
  Nextera Adapter       rc Index 2 (i5) Adapter [E/H/N/S]507
```

Therefore, although clipped/trimmed, these FASTQ files still contains putative alien oligonucleotide sequences. Interestingly, one can notice that their prefixes are not fully identical to the [Nextera Adapter](https://support-docs.illumina.com/SHARE/AdapterSeq/Content/SHARE/AdapterSeq/Nextera/SequencesNXTILMPrepAndPCR.htm), therefore showing the usefulness of a tool able to directly infer (without any prior knowledge) putative alien oligonucleotide sequences. 
