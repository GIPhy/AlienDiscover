/*
  ########################################################################################################

  AlienDiscover: inferring alien oligonucleotide sequence(s) (adapters, primers, ...) from FASTQ file
    
  Copyright (C) 2023  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.zip.*;

public class AlienDiscover {

    //### constants  ################################################################
    final static String VERSION = "0.1.230612ac                               Copyright (C) 2023 Institut Pasteur";
    static final String NOTHING = "N.o./.T.h.I.n.G";
    static final    int BUFFER  = 1<<16;
    static final   byte B0      = (byte) 0;
    static final   byte B_1     = (byte) -1;
    static final    int I256    = 256;
    static final   byte UTF8_A  = (byte) 65;
    static final   byte UTF8_C  = (byte) 67;
    static final   byte UTF8_G  = (byte) 71;
    static final   byte UTF8_T  = (byte) 84;
    static final   byte UTF8_a  = (byte) 97;
    static final   byte UTF8_c  = (byte) 99;
    static final   byte UTF8_g  = (byte) 103;
    static final   byte UTF8_t  = (byte) 116;
    static final   byte K       = 15;                      //## NOTE: k-mer length
    static final   byte K_1     = (byte) (K + B_1);
    static final    int F0      = 1 << (2 * K);            //## NOTE: == 4^k
    static final   byte BSIZE   = (byte) 2;                //## NOTE: no. bits per nucleotides
    static final   byte BA      = (byte) 0;                //## NOTE: == °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°00
    static final   byte BC      = (byte) 1;                //## NOTE: == °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°01
    static final   byte BG      = (byte) 2;                //## NOTE: == °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°10
    static final   byte BT      = (byte) 3;                //## NOTE: == °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°11
    static final    int MSK     = (1 << (BSIZE * K)) - 1;  //## NOTE: == °°111111111111111111111111111111 == 2*k 1s
    static final    int POLY_A  = 0;                       //## NOTE: == °°000000000000000000000000000000
    static final    int POLY_C  = 357913941;               //## NOTE: == °°010101010101010101010101010101
    static final    int POLY_G  = 715827882;               //## NOTE: == °°101010101010101010101010101010
    static final    int POLY_T  = 1073741823;              //## NOTE: == °°111111111111111111111111111111
    
    //### options   #################################################################
    static    File infile;  // -i
    static     int minlgt;  // -l
    static     int mseq1;   // -m
    static     int mseq2;   // -n
    static  double spos;    // -s
    static boolean verbose; // -v

    //### io   ######################################################################
    static         String filename;
    static BufferedReader in;
    static BufferedWriter out;

    //### data   ####################################################################
    static             int rlgt;                 // (max) read length
    static final    byte[] kcard = new byte[F0]; // no. occurence of each k-mer
    static             int thr;                  // minimum no. occurence to assess alien k-mers
    static          BitSet akmr;                 // alien k-mers
    static TreeSet<String> oligos;               // alien oligos

    //### stuffs   ##################################################################
    static          boolean again, succ, once255;
    static             byte skip;
    static              int b, c, i, j, o, x, y, lgt, kmr, min, max;
    static             long cpt;
    static           String oligo, seq;
    static           byte[] ba;
    static final     long[] la = new long[I256];
    static TreeSet<Integer> tskmr;
    
    public static void main(String[] args) throws IOException {

	//#############################################################################################################
	//#############################################################################################################
	//### doc                                                                                                   ###
	//#############################################################################################################
	//#############################################################################################################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println("AlienDiscover v" + VERSION);
	    System.out.println("");
	    System.out.println(" Fast inference of alien oligonucleotide sequence(s) (adapters, primers, ...) from FASTQ file");
	    System.out.println("");
	    System.out.println(" USAGE:");
	    System.out.println("   AlienRemover -i <FASTQ>  [-m <int>]  [-s <float>]  [-n <int>]  [-l <int>]  [-v]  [-h]");
	    System.out.println("");
	    System.out.println(" OPTIONS:");
	    System.out.println("    -i <infile>   FASTQ-formatted  input file  (mandatory);  filename should end");
	    System.out.println("                  with .gz when gzipped (mandatory)");
	    System.out.println("    -m <integer>  number of FASTQ blocks  to read to determine  the maximum read");
	    System.out.println("                  length (default: 1000)");
	    System.out.println("    -s <float>    position quantile to start enumerating k-mers (default: 0.75)");
	    System.out.println("    -n <integer>  number of FASTQ blocks  to read to look for the alien sequence");
	    System.out.println("                  (default: 100000)");
	    System.out.println("    -l <integer>  minimum length of the inferred alien sequence(s) (default: 40)");
	    System.out.println("    -v            verbose mode (default: not set)");
	    System.out.println("    -h            prints this help and exit");
	    System.out.println("");
	    if ( args.length == 0 ) System.exit(1);
	    System.exit(0);
	}


	//#############################################################################################################
	//#############################################################################################################
	//### reading options                                                                                       ###
	//#############################################################################################################
	//#############################################################################################################
	infile = new File(NOTHING);  // -i
	minlgt = 40;                 // -l
	mseq1 = 1000;                // -m
	mseq2 = 100000;              // -n
	spos = 0.75;                 // -s
	verbose = false;             // -v
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") )     {  infile = new File(args[++o]);           continue; }
	    if ( args[o].equals("-l") ) try {  minlgt = Integer.parseInt(args[++o]);   continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -l)"); System.exit(1); }
	    if ( args[o].equals("-m") ) try {   mseq1 = Integer.parseInt(args[++o]);   continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -m)"); System.exit(1); }
	    if ( args[o].equals("-n") ) try {   mseq2 = Integer.parseInt(args[++o]);   continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -n)"); System.exit(1); }
	    if ( args[o].equals("-s") ) try {    spos = Double.parseDouble(args[++o]); continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -s)"); System.exit(1); }
	    if ( args[o].equals("-v") )     { verbose = true;                          continue; }
	}
	filename = infile.toString();
	if ( filename.equals(NOTHING) ) { System.err.println("no input file (option -i)");                                                System.exit(1); }
	if ( ! infile.exists() )        { System.err.println("file " + filename + " does not exist (option -i)");                         System.exit(1); }
	if ( minlgt < 15 )              { System.err.println("alien oligo length should be at least 15 (option -l): " + minlgt);          System.exit(1); }
	if ( mseq1 < 1 )                { System.err.println("no. sequenced reads should be at least 1 (option -m): " + mseq1);           System.exit(1); }
	if ( mseq2 < 1000 )             { System.err.println("no. sequenced reads should be at least 1000 (option -n): " + mseq2);        System.exit(1); }
	if ( spos < 0 )                 { System.err.println("starting position quantile should be positive (option -s): " + spos);       System.exit(1); }
	if ( spos > 0.9 )               { System.err.println("starting position quantile should be lower than 0.9 (option -s): " + spos); System.exit(1); }


	//#############################################################################################################
	//#############################################################################################################
	//### parsing infile                                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	if ( verbose ) System.err.println("AlienDiscover v" + VERSION.replaceAll("\\s{3,}", "   "));
	in = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	rlgt = 0;
	c = 0;
	while ( in.readLine() != null && ++c <= mseq1 ) {
	    seq = in.readLine();  //## NOTE: sequenced read
	    in.readLine(); 
	    in.readLine();        //## NOTE: Phred scores
	    if ( (lgt=seq.length()) > rlgt ) rlgt = lgt;
	}
	if ( verbose ) System.err.println("read length = " + rlgt + " bps (from " + (--c) + " reads)");
	in.close();
	
	in = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	tskmr = new TreeSet<Integer>();
	again = true;
	once255 = false;
	c = 0;
	while ( again && in.readLine() != null ) {
	    seq = in.readLine();  //## NOTE: sequenced read
	    in.readLine(); 
	    in.readLine();        //## NOTE: Phred scores

	    if ( (lgt=seq.length()) < K ) continue;

	    b = (int) (spos * rlgt);
	    if ( lgt - b < K ) continue;
	    ++c;

	    ba = seq.getBytes();
	    tskmr.clear();
	    kmr = skip = B0;
	    i = -1;
	    while ( ++i < K_1 ) {
		++b;
		kmr <<= BSIZE; skip = ( skip > B0 ) ? --skip : skip;
		switch ( ba[b] ) { 
		case UTF8_A: case UTF8_a: /*kmr |= BA;*/ continue;
		case UTF8_C: case UTF8_c:   kmr |= BC;   continue;
		case UTF8_G: case UTF8_g:   kmr |= BG;   continue;
		case UTF8_T: case UTF8_t:   kmr |= BT;   continue;
		default:                    skip = K;    continue; 
		}
	    }
	    --b;
	    while ( ++b < lgt ) {
		kmr <<= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
		switch ( ba[b] ) { 
		case UTF8_A: case UTF8_a: /*kmr |= BA;*/ break;
		case UTF8_C: case UTF8_c:   kmr |= BC;   break;
		case UTF8_G: case UTF8_g:   kmr |= BG;   break;
		case UTF8_T: case UTF8_t:   kmr |= BT;   break;
		default:                    skip = K;    continue; 
		}
		
		if ( skip > B0 ) continue;
		kmr &= MSK;
		if ( kmr == POLY_A || kmr == POLY_C || kmr == POLY_G || kmr == POLY_T ) continue;
		if ( ! tskmr.add(kmr) ) continue;          //## NOTE: kmr already present in seq => continue
		
		if ( once255 && kcard[kmr] == B_1 ) {      //## NOTE: maximum occurrence value (:= 255) already reached once => break
		    again = false;
		    break;
		}
		if ( ++kcard[kmr] == B_1 ) once255 = true; //## NOTE: maximum occurrence value (:= 255) reached for the first time
	    }
	}
	in.close();

	
	//#############################################################################################################
	//#############################################################################################################
	//### looking for outlier k-mers                                                                            ###
	//#############################################################################################################
	//#############################################################################################################
	//### looking for the (rlgt-K) most occurring k-mers #####
	cpt = 0;
	for (byte occ: kcard) {
	    cpt += ( occ > 0 ) ? 1 : 0;
	    ++la[b2i(occ)]; //## NOTE: la[occ] = no. k-mers observed occ times
	}
	if ( verbose ) System.err.println("no. distinct k-mers = " + cpt + " (from " + c + " reads)");
	rlgt -= K;
	cpt = 0; thr = I256; while ( --thr > 0 && (cpt+=la[thr]) < rlgt ) {}
	++thr;
	if ( verbose ) System.err.println("alien k-mers > " + thr + " occurrences");
	//### assessing outlier k-mers, i.e. those occurring > thr times #####
	akmr = new BitSet(F0);
	kmr = -1; for (byte occ: kcard) { ++kmr; if ( b2i(occ) > thr ) akmr.set(kmr); }
	if ( verbose ) System.err.println("no. alien k-mers = " + akmr.cardinality());
	

	//#############################################################################################################
	//#############################################################################################################
	//### looking for alien oligo(s)                                                                            ###
	//#############################################################################################################
	//#############################################################################################################
	if ( verbose ) System.err.println("searching alien oligos ...");
	in = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	oligos = new TreeSet<String>(); 
	max = minlgt+1;           //## NOTE: max := lgt of the largest alien oligo(s)
	min = 3*rlgt/4;           //## NOTE: min := min read lgt to parse
	c = 0;
	while ( in.readLine() != null && ++c <= mseq2 ) {
	    seq = in.readLine();  //## NOTE: sequenced read
	    in.readLine(); 
	    in.readLine();        //## NOTE: Phred scores

	    if ( (lgt=seq.length()) < min ) { --c; continue; }
	    
	    ba  = seq.getBytes();
	    kmr = skip = B0;
	    b = -1;
	    while ( ++b < K_1 ) {
		kmr <<= BSIZE; skip = ( skip > B0 ) ? --skip : skip;
		switch ( ba[b] ) { 
		case UTF8_A: case UTF8_a: /*kmr |= BA;*/ continue;
		case UTF8_C: case UTF8_c:   kmr |= BC;   continue;
		case UTF8_G: case UTF8_g:   kmr |= BG;   continue;
		case UTF8_T: case UTF8_t:   kmr |= BT;   continue;
		default:                    skip = K;    continue; 
		}
	    }
	    
	    i = j = 0;  //## NOTE: seq.substring(i, j) is the largest alien oligo 
	    x = y = 0;  //## NOTE: seq.substring(x, y) is a putative alien oligo  
	    succ = false;
	    --b;
	    while ( ++b < lgt ) {
		kmr <<= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
		switch ( ba[b] ) { 
		case UTF8_A: case UTF8_a: /*kmr |= BA;*/ break;
		case UTF8_C: case UTF8_c:   kmr |= BC;   break;
		case UTF8_G: case UTF8_g:   kmr |= BG;   break;
		case UTF8_T: case UTF8_t:   kmr |= BT;   break;
		default:                    skip = K;    break; 
		}
		
		if ( skip > B0 ) {                          //## NOTE: no A, C, G nor T at position b
		    if ( succ ) {                           //## NOTE: b-1 was a match => end of succ
			y = b;                              //## NOTE: seq.substring(x, y) is an alien oligo  
			if ( j-i < y-x ) { i = x; j = y; }  //## NOTE: seq.substring(i, j) is the largest alien oligo  
			succ = false;
		    }
		    continue;
		}
		
		if ( akmr.get(kmr&MSK) ) {                  //## NOTE: alien k-mer match
		    if ( succ ) continue;                   //## NOTE: b-1 was also a match => continue
		    x = b - K_1;
		    succ = true;
		    continue;		    
		}
		//                                          //## NOTE: no alien k-mer match
		if ( ! succ ) continue;                     //## NOTE: b-1 was also a non-match => continue
		y = b;                                      //## NOTE: seq.substring(x, y) is an alien oligo  
		if ( j-i < y-x ) { i = x; j = y; }          //## NOTE: seq.substring(i, j) is the largest alien oligo  
		succ = false;
	    }
	    if ( succ ) {                                   //## NOTE: last position was a match 
		y = b;
		if ( j-i < y-x ) { i = x; j = y; }
	    }		

	    lgt = j - i;                                    //## NOTE: lgt = length of the largest alien oligo in the current seq
	    if ( lgt < max ) continue;

	    oligo = seq.substring(i+1, j);                  //## NOTE: discarding the first base as it is not alien most of the times
	    if ( verbose && ! oligos.contains(oligo) )
		System.err.println(oligo.length() + " bps: \t" + oligo);

	    if ( lgt > max ) {
		oligos.clear();
		max = lgt;
	    }
	    oligos.add(oligo);              
	}
	in.close();
	if ( verbose ) System.err.println("no. parsed reads (> " + (--min) + " bps) = " + (--c));

	
	//#############################################################################################################
	//#############################################################################################################
	//### outputing alien oligo(s)                                                                              ###
	//#############################################################################################################
	//#############################################################################################################
	if ( oligos.size() == 1 ) {
	    System.out.println(">alien " + infile.getName());
	    System.out.println(oligos.first());
	}
	else {
	    b = 0;
	    for (String s: oligos) {
		System.out.println(">alien." + (++b) + " " + infile.getName());
		System.out.println(s);  
	    }
	}
    }

    final static int b2i(final byte b) { return ( b < B0 ) ? I256+b : b; }
}

